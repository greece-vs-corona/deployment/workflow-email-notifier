import logging
import os
import smtplib
import ssl
import sys
from email.message import EmailMessage

logging.basicConfig(level=logging.DEBUG)

smtp_server = os.environ.get('MAIL_SERVER')
port = int(os.environ.get('PORT'))
sender_email = os.environ.get('SENDER_EMAIL')
receiver_email = os.environ.get('RECEIVER_EMAIL')
password = os.environ.get("PASSWORD")
referral_num = os.environ.get("REFERRAL_NUM")

workflow_type = sys.argv[1]
workflow_status = sys.argv[2]
workflow_duration = sys.argv[3]
workflow_failures = sys.argv[4]


minutes, _ = divmod(float(workflow_duration), 60)
hours, minutes= divmod(minutes, 60)


context = ssl.create_default_context()
msg = EmailMessage()
msg['Subject'] = "Workflow " + referral_num + " " + workflow_status
msg['From'] = sender_email
msg['To'] = receiver_email

email_msg = "Workflow with referral number: '" + referral_num + "' and type: '" + workflow_type + "' finished with status: '" + workflow_status + "'.\r\n\r\nTotal duration was: " + hours + " hours and " + minutes + " minutes." 

if workflow_failures != 'null':
    email_msg += "\r\n\r\nThe following errors occurred: " + workflow_failures

email_msg += "\r\n\r\nPlease do not reply to this message."

msg.set_content(email_msg)

with smtplib.SMTP(smtp_server, port) as server:
    server.starttls(context=context)
    server.login(sender_email, password)
    server.send_message(msg)
